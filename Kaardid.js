/**
 * Created by Urmas on 29.01.2017.
 */
'use strict';

//Juhuslik kaart

//Nupp, millele vajutades kuvatakse lehel üks suvaline kaart kaardipakist.

function yksKaart() {
    var kuva = document.getElementById("tulemus-div");
    var button = document.getElementById("b1");
    kuva.style.display = 'block';

    var kaardipakk = new Array();
    var kaardipakk = ["ri6 ", "ri7 ", "ri8 ", "ri9 ", "ri10", "riP ", "riE ", "riK ", "riÄ ",
        "ru6 ", "ru7 ", "ru8 ", "ru9 ", "ru10", "ruP ", "ruE ", "ruK ", "ruÄ ",
        "po6 ", "po7 ", "po8 ", "po9 ", "po10", "poP ", "poE ", "poK ", "poÄ ",
        "är6 ", "är7 ", "är8 ", "är9 ", "är10", "ärP ", "ärE ", "ärK ", "ärÄ "];
    
    button.onclick = function () {
        var juhuslikKaart = kaardipakk[Math.floor((Math.random() * 35) + 0)];
        console.log(juhuslikKaart);

        var mast = juhuslikKaart[0]+juhuslikKaart[1];
        console.log(mast);
        var väärtus = juhuslikKaart[2]+juhuslikKaart[3];
        console.log(väärtus);

        var varvus = undefined;

        if (mast == 'ri'){
            mast = 'Risti ';
            varvus = "black";       //must
        }
        if (mast == "ru"){
            mast = "Ruutu ";
            varvus = "#ff0000";     //punane
        }
        if (mast == "po"){
            mast = "Poti ";
            varvus = "black";       //must
        }
        if (mast == "är"){
            mast = "Ärtu ";
            varvus = "#ff0000";     //punane
        }
        if (väärtus == "P "){
            väärtus = "poiss";
        }
        if (väärtus == "E "){
            väärtus = "emand";
        }
        if (väärtus == "K "){
            väärtus = "kuningas";
        }
        if (väärtus == "Ä "){
            väärtus = "äss";
        }

        kuva.innerHTML = mast+väärtus;

        console.log(varvus)
        console.log(mast)

        myFunction(varvus)
    }
}

function myFunction(varvus) {
    document.getElementById("tulemus-div").style.color = varvus;
}
