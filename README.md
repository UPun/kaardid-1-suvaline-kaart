"# My project's README"

JavaScripti harjutamiseks koostasin programmi, mis loob massiivi 'kaardipakk', mis koosneb 36 kaardist.
Klikates nupule 'Üks suvaline kaart kaardipakist', valitaks massiivist 'kaardipakk' juhuslik kaart ning kuvatakse.
Kui juhuslikult valitud kaardiks osutus punast masti kaart (ärtu või ruutu), siis värvitakse valitud kaardi kirje punaseks. Musta värvi kaardid (risti ja poti), nende kirje värvitakse mustaks.

Head katsetamist!